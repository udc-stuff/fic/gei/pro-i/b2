#include <stdio.h>

int main() {
	const float IRPF = 0.15f;
	const float IRPF_PER_CHILD = 0.1f;
	const int MAX_CHILDREN = 5;

	int children;
	float salary;
	float tmp;

	printf("Introduzca su salario bruto anual: ");
	scanf("%f", &salary);
	printf("Introduzca el numero de hijos menores de edad: ");
	scanf("%d", &children);

	tmp = IRPF_PER_CHILD * (children > MAX_CHILDREN? MAX_CHILDREN: children);

	printf("El IRPF es de %.2f euros\n", salary * IRPF);
	printf("La reduccion debida a hijos a cargo %.2f euros\n", salary * IRPF * tmp);
	printf("Total anual a pagar: %.2f euros\n", salary * IRPF * (1 - tmp));

	return 0;
}
