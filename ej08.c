#include <stdio.h>
#include <stdbool.h>
#include <time.h>

int main() {
	time_t rawtime;
	struct tm *info;

	int year;
	int month, day;
	int aux, cnt;

	bool leap_year;

	time(&rawtime);
	info = gmtime(&rawtime);
	year = 1900 + info->tm_year;

	printf("Escriba una fecha del ano %d (dd mm): ", year);
	scanf("%d %d", &day, &month);

	leap_year = ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0));

	cnt = day;
	switch (month - 1) {
		case 11: cnt += 30;
		case 10: cnt += 31;
		case  9: cnt += 30;
		case  8: cnt += 31;
		case  7: cnt += 31;
		case  6: cnt += 30;
		case  5: cnt += 31;
		case  4: cnt += 30;
		case  3: cnt += 31;
		case  2: cnt += leap_year? 29: 28;
		case  1: cnt += 31;
		case  0: break;
		default:
			cnt =-1;
	}
	
	if (cnt == -1) {
		printf("El mes %d no es valido\n", month);
	} else {
		printf("La fecha introducida corresponde al dia %d del ano en curso\n", cnt);
	}

	return 0;
}
