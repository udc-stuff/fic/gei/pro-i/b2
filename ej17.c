#include <stdio.h>

int main() {
	char c;

	printf("Introduzca un caracter: ");
	scanf("%c", &c);

	if (c >= '0' && c <= '9') {
		printf("%c es un digito\n", c);
	} else if ((c >= 'a' && c <= 'z') || (c >='A' && c <= 'Z')) {
		switch (c) {
			case 'a':
			case 'A':
			case 'e':
                        case 'E':
                        case 'i':
                        case 'I':
                        case 'o':
                        case 'O':
                        case 'u':
                        case 'U':
                        	printf("%c es una vocal\n", c);
				break;
			default:
				printf("%c es una consonante\n", c);
		}

	} else {
		printf("%c es un caracter especial\n", c); 	
	}
	return 0;
}
