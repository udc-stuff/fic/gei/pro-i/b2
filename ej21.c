#include <stdio.h>

int main() {
	const int SUBJECTS = 6;
	
	int cnt; // variable de control del bucle
	int pass; // para el sumatorio de las asignaturas aprobadas
	float grade; // la nota de una asignatura individual
	float total; // el sumatorio de todas las notas
	float aux; // para calcular las medias

	cnt = pass = 0;
	total = 0.0f;
	while (cnt < SUBJECTS) {
		printf("Introduzca calificacion asignatura %d (formato X.X, 0 para No Presentadio): ", cnt + 1);
		scanf("%f", &grade);

		if (grade >= 0.0f && grade <= 10.0f) {
			total += grade;
			if (grade != 0)
				pass++;

			cnt++;
		} else {
			printf("La calificacion tiene que estar entre 0 y 10\n");
		}
	}

	aux = total / cnt;
	printf("Nota media: ");
	switch ((int) aux) {
		case 10:
		case 9:
			printf("SOBRESALIENTE ");
			break;
		case 8:
		case 7:
			printf("NOTABLE ");
			break;
		case 6:
		case 5:
			printf("APROBADO ");
			break;
		default:
			printf("SUSPENSO ");
	}
	printf("(%.2f)\n", aux);
	
	aux = total / pass;
        printf("Nota media asignaturas presentadas: ");
        switch ((int) aux) {
                case 10:
                case 9:
                        printf("SOBRESALIENTE ");
                        break;
                case 8:
                case 7:
                        printf("NOTABLE ");
                        break;
                case 6:
                case 5:
                        printf("APROBADO ");
                        break;
                default:
                        printf("SUSPENSO ");
        }
        printf("(%.2f)\n", aux);

	return 0;
}
