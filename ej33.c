#include <stdio.h>

int main() {
	int number;
	int cnt, total;

	printf("Introduce un numero entero positivo: ");
	scanf("%d", &number);

	if (number < 0) {
		printf("%d: el numero tiene que ser positivo\n", number);
		return 0;
	}

	total = 1;
	for (cnt = 1; cnt <= number; cnt++)
		total *= cnt;

	printf("%d! = %d\n", number, total);

	return 0;
}
