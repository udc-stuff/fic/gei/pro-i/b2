#include <stdio.h>

int main() {
	int number;
	int cnt;
	int n1, n2, n3;

	printf("Introduce un numero entero (N>0): ");
	scanf("%d", &number);

	if (number <= 0) {
		printf("%d: el numero tiene que ser positivo\n", number);
		return 0;
	}

	n1 = 0;
	n2 = 1;
	for (cnt = 2; cnt <= number; cnt++) {
		n3 = n1 + n2;
		n1 = n2;
		n2 = n3;
	}

	printf("El termino %d de la sucesion de fibonacci es %d\n", number, n3);

	return 0;
}
