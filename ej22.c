#include <stdio.h>

int main() {
	const int MAX = 100;

	int even, odd;
	int cnt;

	for (cnt = even = odd = 0; cnt < MAX * 2; cnt ++)
		if (cnt % 2 == 0)
			even += cnt;
		else
			odd += cnt;

	printf("La suma de los primeros 100:\n");
	printf("\t- numeros pares es %d\n", even);
	printf("\t- numeros impares es %d\n", odd);

	return 0;
}
