#include <stdio.h>

int main() {
	char name[20];
	int age;

	printf("Nombre: ");
	scanf("%s", name);

	printf("Edad: ");
	scanf("%d", &age);

	if (age < 32) {
		printf("%s es JOVEN\n", name);
	} else if (age < 75) {
		printf("%s es ADULTA\n", name);
	} else {
		printf("%s es ANCIANA\n", name);
	}

	return 0;
}
