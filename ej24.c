#include <stdio.h>

int main() {
	const int NUMBER = 7;
	const int MAX = 10;

	int cnt;
	for (cnt = 0; cnt <= MAX; cnt++)
		printf("%d x %2d = %2d\n", NUMBER, cnt, NUMBER * cnt);

	return 0;
}
