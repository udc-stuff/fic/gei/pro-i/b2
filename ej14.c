#include <stdio.h>

int main() {
	int n1, n2;

	printf("Introduzca un numero entero positivo de 3 cifras: ");
	scanf("%d", &n1);

	printf("Introduzca un numero entero positivo de 1 cifra: ");
	scanf("%d", &n2);

	if (n1 < 100 || n1 > 999) {
		printf("El primer numero no tiene 3 cifras!!!\n");
		return 0;
	}

	if (n2 < 0 || n2 > 9) {
		printf("El segundo numero no tiene 1 cifra!!!\n");
		return 0;
	} 

	printf(" %3d\n x %d\n====\n%4d\n", n1, n2, n1*n2);

	return 0;
}
