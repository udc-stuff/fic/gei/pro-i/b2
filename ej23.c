#include <stdio.h>

int main() {
	const int MAX = 30;
	const int MIN = 20;

	int aux = MIN - 1;

	while (aux < MIN || aux > MAX) {
		printf("Introduce un numero entre %d y %d: ", MIN, MAX);
		scanf("%d", &aux);
	}

	printf("El numero introducido es: %d\n", aux);

	return 0;
}
