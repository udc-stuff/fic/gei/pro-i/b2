#include <stdio.h>

int main() {
	int l1, l2, l3;

	printf("Introduzca la longitud del primer lado del triangulo (cm): ");
	scanf("%d", &l1);
	printf("Introduzca la longitud del segundo lado del triangulo (cm): ");
	scanf("%d", &l2);
	printf("Introduzca la longitud del tercer lado del triangulo (cm): ");
	scanf("%d", &l3);

	if (l1 == l2 && l1 == l3) {
		printf("El triangulo es EQUILATERO (lado1=lado2=lado3=%d)\n", l1);
	} else if (l1 == l2) {
		printf("El triangulo es ISOSCELES (lado1=lado2=%d)\n", l1);
	} else if (l1 == l3) {
		printf("El triangulo es ISOSCELES (lado1=lado3=%d)\n", l1);
	} else if (l2 == l3) {
		printf("El triangulo es ISOSCELES (lado2=lado3=%d)\n", l2);
	} else {
		printf("El triangulo es ESCALENO\n");
	}
	

	return 0;
}
