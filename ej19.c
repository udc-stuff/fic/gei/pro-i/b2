#include <stdio.h>

int main() {
	int month, days;
	int aux;

	printf("Introduzca el numero del mes del que quiere calcular los dias [1-12]: ");
	scanf("%d", &month);

	if (month < 1 || month > 12) {
		printf("Dato invalido: el mes tiene que ser un numero comprendido entre 1 y 12\n");
		return 0;
	}

	if (month == 2) {
		printf("Introduzca el ano: ");
		scanf("%d", &aux);

		days = ((aux % 4 == 0) && (aux % 100 != 0 || aux % 400 == 0))? 28: 27;
		printf("El mes %d del ano %d tiene %d dias\n", month, aux, days);
	} else {
		aux = month < 8 ? month: (month % 8) + 1;
		days = aux % 2 == 0? 30: 31;
		printf("El mes %d tiene %d dias\n", month, days);
	}

	return 0;
}
