#include <stdio.h>

int main() {
	int n1, n2;

	printf("Introduce dos numeros enteros separados por un espacio: ");
	scanf("%d %d", &n1, &n2);

	if (n1 % n2 == 0)
		printf("El numero %d es divisible por %d\n", n1, n2);
	else
		printf("El numero %d no es divisible por %d\n", n1, n2);

	return 0;
}
