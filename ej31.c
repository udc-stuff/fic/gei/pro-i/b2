#include <stdio.h>

int main() {
	char c;
	int cnt;

	for (cnt = 1801; cnt < 2000; cnt++) {
		if (cnt == 1901) {
			printf("\nPulse enter para continuar...");
			scanf("%c", &c);
		}
		if (cnt % 4 == 0 && (cnt % 100 != 0 || cnt % 400 == 0))
			printf("%d ", cnt);
	}
	printf("\n");

	return 0;
}
