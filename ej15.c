#include <stdio.h>

int main() {
	char c;

	printf("Escoga una opcion:\n");
	printf("\tB - Bebe\n");
	printf("\tA - Adolescente\n");
        printf("\tM - Mujer\n");
        printf("\tH - Hombre\n");
	printf("Opcion: ");
	scanf("%c", &c);

	switch(c) {
		case 'b':
		case 'B':
			printf("BEBE\n");
			break;
		case 'a':
		case 'A':
			printf("ADOLESCENTE\n");
			break;
		case 'm':
		case 'M':
			printf("MUJER\n");
			break;
		case 'h':
		case 'H':
			printf("Hombre\n");
			break;
		default:
			printf("Opcion incorrecta\n");
	
	}

	return 0;
}
