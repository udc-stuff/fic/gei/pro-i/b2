#include <stdio.h>
#include <stdbool.h>
#include <math.h>

// 28 | 2
//   \---
// 0   14 | 2
//        \---
//      0   7 | 2
//            \---
//          1   3 | 2 
//                 \---
//              1   1

// control => para saber si estamos en los primeros zeros
// zeros => cantidad de zeros al inicio
// aux => copia de n para no perder el valor
// tmp => numero binario al reves
// result => donde se vuelca el resultado final

int main() {
	int n, result;
	int aux, tmp, mod;
	int zeros = 0;
	bool control = true;

	printf("Introduce un numero: ");
	scanf("%d", &n);

	aux = n;
	tmp = 0;
	while (aux != 0) {
		mod = aux % 2;
		if (mod == 1)
			control = false;
		else if (mod == 0 && control)
			zeros++;
		
		tmp *= 10;
		tmp += mod;

//		printf("%d", mod);
		aux /= 2;
	}
//	printf("\n");


	// Ahora hay que darle la vuelta al numero
	result = 0;
	while (tmp != 0) {
		result *= 10;
		result += tmp % 2;
		tmp /= 10;
	}
	result *= powf(10, zeros);

	if (n < 0)
		result *= -1;

	printf("%d en binario es %d\n", n, result);

	return 0;
}
