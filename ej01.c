#include <stdio.h>

int main() {
	int number;

	printf("Introduzca un numero entero: ");
	scanf("%d", &number);

	if (number >= 0)
		printf("El numero %d es positivo\n", number);
	else
		printf("El numero %d es negativo\n", number);

	return 0;
}
