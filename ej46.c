#include <stdio.h>

int main() {
	const int MAX = 10;
	int row, col;

	// header
	for (col = 1; col < MAX; col++)
		printf("\t%d", col);
	printf("\n\n");

	for (row = 1; row < MAX; row++) {
		printf("%d", row);
		for (col = 1; col <= row; col++)
                	printf("\t%d", row*col);
        	printf("\n\n");
	}

	return 0;
}
