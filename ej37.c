#include <stdio.h>

int main() {
	int number;
	int cnt, total;
	int n1, n2, n3;

	printf("Introduce un numero entero (N>0): ");
	scanf("%d", &number);

	if (number <= 0) {
		printf("%d: el numero tiene que ser positivo\n", number);
		return 0;
	}


	if (number < 2) {
		total = number;
	} else {
		n1 = 0;
		n2 = 1;
		total = 1;
		for (cnt = 2; cnt <= number; cnt++) {
			n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
			total += n3;
		}
	}

	printf("La suma de los  %d primeros temrinos de la sucesion de fibonacci es %d\n", number, total);

	return 0;
}
