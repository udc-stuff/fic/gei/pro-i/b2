#include <stdio.h>

int main() {
	int cnt;
	float sum, lim;

	printf("Introduzca un limite para la sucesion: ");
	scanf("%f", &lim);

	cnt = 0;
	sum = 0.0f;
	while (sum <= lim) {
		cnt++;
		sum += 1/ (float) cnt;
	}

	printf("Se han realizado %d iteraciones para alcanzar el valor %f (limite=%f)\n", cnt, sum, lim);	

	return 0;
}
