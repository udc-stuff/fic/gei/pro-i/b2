#include <stdio.h>

int main() {
	int n1, n2, n3;
	int high, low;

	printf("Introduzca el primer numero: ");
	scanf("%d", &n1);
	printf("Introduzca el segundo numero: ");
	scanf("%d", &n2);
	printf("Introduzca el tercer numero: ");
	scanf("%d", &n3);


	if (n1 >= n2 && n1 >= n3) {
		high = n1;
	} else if (n2 >= n3 && n2 >= n1) {
		high = n2;
	} else {
		high = n3;
	}

	if (n1 <= n2 && n1 <= n3) {
		low = n1;
	} else if (n2 <= n3 && n2 <= n3) {
		low = n2;
	} else {
		low = n3;
	}

	printf("El mayor y menor numero son %d y %d correspondientemente\n", high, low);
	
	if (low == 0) {
		printf("El divisor es 0 y no se puede realizar la division\n");
		return 0;
	}

	printf("%10d dividido | entre %d\n", high, low);
	printf("                    ----------\n");
	printf("          R: %-7d   C: %d\n", high % low, high / low);
	

	return 0;
		
}
