#include <stdio.h>

int main() {
	const int MAX = 10;

	int cnt, number;

	number = -1;
	while (number < 0 || number > 10) {
		printf("Introduzca un numero entero positivo: ");
		scanf("%d", &number);
	}

	for (cnt = 0; cnt <= MAX; cnt++)
		printf("%d x %2d = %d\n", number, cnt, number * cnt);

	return 0;
}
