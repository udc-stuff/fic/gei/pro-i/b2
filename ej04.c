#include <stdio.h>

int main() {
	int n1, n2, n3;
	int aux;

	printf("Primer numero: ");
	scanf("%d", &n1);
	printf("Segundo numero: ");
	scanf("%d", &n2);
	printf("Tercer numero: ");
	scanf("%d", &n3);

	if (n1 >= n2 && n1 >= n3) {
		aux = n1;
	} else if (n2 >= n1 && n2 >= n3) {
		aux = n2;
	} else {
		aux = n3;
	}

	printf("%d es el numero mayor de los tres introducidos\n", aux);

	return 0;
}
