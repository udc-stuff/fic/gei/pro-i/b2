#include <stdio.h>
#include <math.h>

int main() {
	char name[20];
	int height;
	float weight;
	float imc;
	
	printf("Introduzca usted su nombre: ");
	scanf("%s", name);

	printf("Introduzca altura en centimetros: ");
	scanf("%d", &height);

	printf("Introduzca peso en kilos: ");
	scanf("%f", &weight);

	imc = weight / powf(height/100.0f, 2);

	printf("%s, segun el indice de masa corporal (IMC=%.2f), tiene usted ", name, imc);
	if (imc < 18.5f) {
		printf("un peso por debajo de su salud ");
	} else if (imc < 25.0f) {	
		printf("un peso normal ");
	} else if (imc < 27.0f) {
		printf("sobrepeso de grado I ");
	} else if (imc < 30.0f) {
		printf("sobrepeso de grado II ");
	} else {
		printf("obesidad ");
	}
	printf("para su estatura\n");

	return 0;
}
