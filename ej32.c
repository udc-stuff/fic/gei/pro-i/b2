#include <stdio.h>

int main() {
	int value, power;
	int cnt, total;

	printf("Introduce un numero entero: ");
	scanf("%d", &value);

	printf("Introduce la potencia (positivo): ");
	scanf("%d", &power);

	if (power < 0) {
		printf("%d: la potencia tiene que ser un numero positivo\n", power);
		return 0;
	}

	total = 1;
	for (cnt = 0; cnt < power; cnt++)
		total *= value;

	printf("%d^%d = %d\n", value, power, total);

	return 0;
}
