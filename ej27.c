#include <stdio.h>

int main() {
	const int MAX = 100;

	int cnt, aux;

	printf("BUCLE FOR: ");
	for (cnt =  aux = 0; cnt < MAX; cnt++)
		aux += cnt;
	printf("%d\n", aux);

	printf("BUCLE WHILE: ");
	cnt =  aux = 0;
	while (cnt < MAX) {
		aux += cnt;
		cnt++;
	}
	printf("%d\n", aux);

	printf("BUCLE DO WHILE: ");
        cnt =  aux = 0;
        do {
                aux += cnt;
                cnt++;
        } while (cnt < MAX);
        printf("%d\n", aux);

	return 0;
}
