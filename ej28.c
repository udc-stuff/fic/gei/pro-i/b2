#include <stdio.h>

int main() {
	int aux, total;

	total = 0;
	aux = -1;

	while (aux != 0) {
		printf("Introduce un numero (0 para salir): ");
		scanf("%d", &aux);

		total += aux;
	}

	printf("La suma de los numeros introducidos es %d\n", total);

	return 0;
}
