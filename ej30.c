#include <stdio.h>

int main() {
	int aux, even, odd;

	even = odd = 0;
	aux = -1;

	while (aux != 0) {
		printf("Introduce un numero (0 para salir): ");
		scanf("%d", &aux);

		if (aux % 2 == 0)
			even += aux;
		else
			odd += aux;
	}

	printf("La suma de los numeros pares es %d\n", even);
	printf("La suma de los numeros impares es %d\n", odd);

	return 0;
}
