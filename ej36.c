#include <stdio.h>

int main() {
	char c = ' ';

	while (c != 'A' && c != 'a') {
		printf("\n");
		printf("R) Dibujar un rectangulo\n");
		printf("C) Dibujar un cuadrado\n");
		printf("P) Dibujar un pentagono\n");
		printf("A) Acabar el programa\n");
		printf("Opcion: ");
		scanf(" %c", &c);

		switch (c) {
			case 'R':
			case 'r':
				printf("Ahora dibujaria un rectangulo\n");
				break;
			case 'C':
			case 'c':
				printf("Ahora dibujaria un cuadrado\n");
				break;
			case 'P':
			case 'p':
				printf("Ahora dibujaria un pentagono\n");
				break;
			case 'A':
			case 'a':
				printf("Ahora saldre del programa\n");
				break;
			default:
				printf("%c: opcion incorrecta\n", c);
		}
	}

	return 0;
}
