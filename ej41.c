#include <stdio.h>

int main() {
	const int MAX = 1000;
	int x, y, z;

	for (x = 0; x <= MAX; x++)
		for (y = 0; y <= MAX; y++)
			for (z = (x<y)? x: y; z <= MAX; z++)
				if (x*x + y*y == z*z)
					printf("%d^2 + %d^2 = %d^2\n", x, y, z);

	return 0;
}
