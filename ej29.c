#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void print_array(int size, float a[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%.3f%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}


/**
 * Esta funcion leee una cadena de entrada repleta de numeros y los introduce
 * dentor de un array
 * str: cadena de texto que contiene los numeros
 * a: array donde almacenar los numeros
 * size: tamano maximo del array
 *
 * return la funcion devuelve la cantidad de numeros que se han introducido
 * dentro del array
 */
int str_to_floats(char *str, float *a, int size) {
	char delim[] ={" \t\n"}; // separadores que se usan dentro del str
	char *token;

	int cnt = 0;

	while ((token = strsep(&str, delim)) != NULL) {
		//printf("TOKEN %d <%s>\n", cnt, token);
		if (strcmp(token, "")) {
			a[cnt] = strtof(token, NULL);
			cnt++;
		}

		if (cnt == size) // No se pueden almacenar mas numeros dentro del array
			return cnt;
	}
	
	return cnt;
}

/**
 * Funcion que calcula la media de los valores de los numeros obtenidos en un
 * array de tamano size
 * array: array de numeros flotantes
 * size: cantidad de numeros a tener en cuentra dentro del array
 * return la media aritmetrica
 */
float get_average(int size, float *array) {
	int cnt;
	float aux = 0.0f;
	for (cnt = 0 ; cnt < size; cnt++)
		aux += array[cnt];

	return aux/size;
}

int main() {
	const int MAX_NUM = 10;
	const int MAX_STR = 100;
	float numbers[MAX_NUM];
	char buffer[MAX_STR];
	int size;
	printf("Introduce todos los numeros flotantes (MAX %d).\n"
	       "Los numeros han de estar separados por espacios.\n"
	       "Para finalizar pulse <enter>: \n", MAX_NUM);

	fgets(buffer, MAX_STR, stdin);

	size = str_to_floats(buffer, numbers, MAX_NUM);
	//print_array(size, numbers);	

	printf("La media aritmetrica es: %.3f\n", get_average(size, numbers));

	return 0;
}
