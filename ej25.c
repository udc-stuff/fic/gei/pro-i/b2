#include <stdio.h>

int main() {
	int row, col;
	for (row = 0; row <= 10; row++) {
		for (col = 1; col <= 5; col++)
			printf("%2d x %2d = %2d\t", col, row, col*row);
		printf("\n");
	}

	return 0;
}
