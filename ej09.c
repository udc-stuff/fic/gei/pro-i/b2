#include <stdio.h>
#include <time.h>

int main() {
	time_t rawtime;
	struct tm *info;

	int current_year, current_month, current_day;
	int year, month, day;
	int aux;

	time(&rawtime);
	info = gmtime(&rawtime);

	current_year = 1900 + info->tm_year;
	current_month = 1 + info->tm_mon;
	current_day = info->tm_mday;	

	printf("Fecha actual: %02d/%02d/%4d\n", current_day, current_month, current_year);
	printf("Introduzca su fecha de cumpeanos (dd/mm/yyyy): ");
	scanf("%d/%d/%d", &day, &month, &year);

	aux = current_year - year;
	if (current_month > month)
		aux--;
	else if (current_month == month && current_day < day)
		aux--; 

	if (aux < 0) 
		printf("La fecha introducida es invalida: %02d/%02d/%04d (actual %02d/%02d/%04d)\n",
		        day, month, year, current_day, current_month, current_year);
	else
		printf("Tienes %d anos\n", aux);

	return 0;
}
