En este repositorio os encontraréis ejercicios del boletín 2 que se han realizado en clase de prácticas.

Quizás encontréis alguna pequeña modificación con respecto al enunciado. Recordar que esas pequeñas modificaciones han sido realizadas para explicar cosas concretas que ocurren al usar el lenguaje C.

# Ej 01.

```c
#include <stdio.h>

int main() {
	int number;

	printf("Introduzca un numero entero: ");
	scanf("%d", &number);

	if (number >= 0)
		printf("El numero %d es positivo\n", number);
	else
		printf("El numero %d es negativo\n", number);

	return 0;
}
```

# Ej 02.

```c
#include <stdio.h>

int main() {
	int number;

	printf("Introduzca un numero entero: ");
	scanf("%d", &number);

	if (number % 2 == 0)
		printf("El numero %d es par\n", number);
	else
		printf("El numero %d es impar\n", number);

	return 0;
}
```

# Ej 03.

```c
#include <stdio.h>

int main() {
	int n1, n2;

	printf("Introduce dos numeros enteros separados por un espacio: ");
	scanf("%d %d", &n1, &n2);

	if (n1 % n2 == 0)
		printf("El numero %d es divisible por %d\n", n1, n2);
	else
		printf("El numero %d no es divisible por %d\n", n1, n2);

	return 0;
}
```

# Ej 04.

```c
#include <stdio.h>

int main() {
	int n1, n2, n3;
	int aux;

	printf("Primer numero: ");
	scanf("%d", &n1);
	printf("Segundo numero: ");
	scanf("%d", &n2);
	printf("Tercer numero: ");
	scanf("%d", &n3);

	if (n1 >= n2 && n1 >= n3) {
		aux = n1;
	} else if (n2 >= n1 && n2 >= n3) {
		aux = n2;
	} else {
		aux = n3;
	}

	printf("%d es el numero mayor de los tres introducidos\n", aux);

	return 0;
}
```

# Ej 05.

```c
#include <stdio.h>

int main() {
	int n1, n2, n3;

	printf("Primer numero: ");
	scanf("%d", &n1);
	printf("Segundo numero: ");
	scanf("%d", &n2);
	printf("Tercer numero: ");
	scanf("%d", &n3);

	if (n1<=n2 && n2<=n3){
		printf("%d<=%d<=%d\n",n1,n2,n3);
	} else if (n1<=n3 && n3<=n2){
		printf("%d<=%d<=%d\n",n1,n3,n2);
	} else if (n2<=n1 && n1<=n3){
		printf("%d<=%d<=%d\n",n2,n1,n3);
	} else if (n2<=n3 && n3<=n1){
		printf("%d<=%d<=%d\n",n2,n3,n1);
	} else if (n3<=n2 && n2<=n1){
		printf("%d<=%d<=%d\n",n3,n2,n1);
	} else if (n3<=n1&& n1<=n2)  {
		printf("%d<=%d<=%d\n",n3,n1,n2);
	}

	return 0;
}
```

# Ej 06.

```c
#include <stdio.h>

int main() {
	int question, correct;
	float aux;

	printf("Cantidad total de preguntas: ");
	scanf("%d", &question);
	printf("Cantidad de preguntas correctas: ");
	scanf("%d", &correct);

	aux = (float) correct/question;

	printf("Porcentaje de respuestas correctas %.2f\n", aux * 100);
	if (aux >= 0.9f){
		printf("Nivel maximo (>=90%%)\n");
	}  else if (aux >= 0.75f){
		printf("Nivel medio (>=75%% y <90%%)\n");
	} else if (aux >= 0.5f){
		printf("Nivel regular (>=50%% y < 75%%)\n");
	} else {
		printf("Fuera de nivel (<50%%)\n");
	}

	return 0;
}
```

# Ej 07.

```c
#include <stdio.h>
#include <math.h>

int main() {
	float a, b, c;
	float aux;

	printf("a*x*x + b*x + c = 0\n");
	printf("Valor del termino a: ");
	scanf("%f", &a);
	printf("Valor del termino b: ");
	scanf("%f", &b);
	printf("Valor del termino c: ");
	scanf("%f", &c);

	aux = powf(b, 2) - 4 * a * c;
	
	switch ((int) aux) {
		case 0:
			printf("Raiz real doble\n");
			printf("\t%f\n", -b/(2*a));
			break;
		default:
			if (aux > 0) {
				printf("Real y distinta\n");
				printf("\t%f\n", (-b + sqrtf(aux))/(2*a) );
				printf("\t%f\n", (-b - sqrtf(aux))/(2*a) );
			} else {
				printf("Compleja\n");
				printf("\t%f + (%f)i\n", -b/(2*a), sqrtf(-aux)/(2*a) );
				printf("\t%f - (%f)i\n", -b/(2*a), sqrtf(-aux)/(2*a) );
			}
	}	


	return 0;
}
```

# Ej 08.

```c
#include <stdio.h>
#include <stdbool.h>
#include <time.h>

int main() {
	time_t rawtime;
	struct tm *info;

	int year;
	int month, day;
	int aux, cnt;

	bool leap_year;

	time(&rawtime);
	info = gmtime(&rawtime);
	year = 1900 + info->tm_year;

	printf("Escriba una fecha del ano %d (dd mm): ", year);
	scanf("%d %d", &day, &month);

	leap_year = ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0));

	cnt = day;
	switch (month - 1) {
		case 11: cnt += 30;
		case 10: cnt += 31;
		case  9: cnt += 30;
		case  8: cnt += 31;
		case  7: cnt += 31;
		case  6: cnt += 30;
		case  5: cnt += 31;
		case  4: cnt += 30;
		case  3: cnt += 31;
		case  2: cnt += leap_year? 28: 27;
		case  1: cnt += 31;
		case  0: break;
		default:
			cnt =-1;
	}
	
	if (cnt == -1) {
		printf("El mes %d no es valido\n", month);
	} else {
		printf("La fecha introducida corresponde al dia %d del ano en curso\n", cnt);
	}

	return 0;
}
```

# Ej 09.

```c
#include <stdio.h>
#include <time.h>

int main() {
	time_t rawtime;
	struct tm *info;

	int current_year, current_month, current_day;
	int year, month, day;
	int aux;

	time(&rawtime);
	info = gmtime(&rawtime);

	current_year = 1900 + info->tm_year;
	current_month = 1 + info->tm_mon;
	current_day = info->tm_mday;	

	printf("Fecha actual: %02d/%02d/%4d\n", current_day, current_month, current_year);
	printf("Introduzca su fecha de cumpeanos (dd/mm/yyyy): ");
	scanf("%d/%d/%d", &day, &month, &year);

	aux = current_year - year;
	if (current_month > month)
		aux--;
	else if (current_month == month && current_day < day)
		aux--; 

	if (aux < 0) 
		printf("La fecha introducida es invalida: %02d/%02d/%04d (actual %02d/%02d/%04d)\n",
		        day, month, year, current_day, current_month, current_year);
	else
		printf("Tienes %d anos\n", aux);

	return 0;
}
```

# Ej 10.

```c
#include <stdio.h>

int main() {
	const float IRPF = 0.15f;
	const float IRPF_PER_CHILD = 0.1f;
	const int MAX_CHILDREN = 5;

	int children;
	float salary;
	float tmp;

	printf("Introduzca su salario bruto anual: ");
	scanf("%f", &salary);
	printf("Introduzca el numero de hijos menores de edad: ");
	scanf("%d", &children);

	tmp = IRPF_PER_CHILD * (children > MAX_CHILDREN? MAX_CHILDREN: children);

	printf("El IRPF es de %.2f euros\n", salary * IRPF);
	printf("La reduccion debida a hijos a cargo %.2f euros\n", salary * IRPF * tmp);
	printf("Total anual a pagar: %.2f euros\n", salary * IRPF * (1 - tmp));

	return 0;
}
```

# Ej 11.

```c
#include <stdio.h>
#include <math.h>

int main() {
	float a, b, c;
	float aux;

	printf("a*x*x + b*x + c = 0\n");
	printf("Valor del termino a: ");
	scanf("%f", &a);
	printf("Valor del termino b: ");
	scanf("%f", &b);
	printf("Valor del termino c: ");
	scanf("%f", &c);

	aux = powf(b, 2) - 4 * a * c;
	
	switch ((int) aux) {
		case 0:
			printf("Raiz real doble\n");
			printf("\t%f\n", -b/(2*a));
			break;
		default:
			if (aux > 0) {
				printf("Real y distinta\n");
				printf("\t%f\n", (-b + sqrtf(aux))/(2*a) );
				printf("\t%f\n", (-b - sqrtf(aux))/(2*a) );
			} else {
				printf("Compleja\n");
				printf("\t%f + (%f)i\n", -b/(2*a), aux/powf(2*a, 2) );
				printf("\t%f - (%f)i\n", -b/(2*a), aux/powf(2*a, 2) );
			}
	}	


	return 0;
}
```

# Ej 12.

```c
#include <stdio.h>

int main() {
	int n1, n2, n3;
	int high, low;

	printf("Introduzca el primer numero: ");
	scanf("%d", &n1);
	printf("Introduzca el segundo numero: ");
	scanf("%d", &n2);
	printf("Introduzca el tercer numero: ");
	scanf("%d", &n3);


	if (n1 >= n2 && n1 >= n3) {
		high = n1;
	} else if (n2 >= n3 && n2 >= n1) {
		high = n2;
	} else {
		high = n3;
	}

	if (n1 <= n2 && n1 <= n3) {
		low = n1;
	} else if (n2 <= n3 && n2 <= n3) {
		low = n2;
	} else {
		low = n3;
	}

	printf("El mayor y menor numero son %d y %d correspondientemente\n", high, low);
	
	if (low == 0) {
		printf("El divisor es 0 y no se puede realizar la division\n");
		return 0;
	}

	printf("%10d dividido | entre %d\n", high, low);
	printf("                    ----------\n");
	printf("          R: %-7d   C: %d\n", high % low, high / low);
	

	return 0;
		
}
```

# Ej 13.

```c
#include <stdio.h>

int main() {
	int l1, l2, l3;

	printf("Introduzca la longitud del primer lado del triangulo (cm): ");
	scanf("%d", &l1);
	printf("Introduzca la longitud del segundo lado del triangulo (cm): ");
	scanf("%d", &l2);
	printf("Introduzca la longitud del tercer lado del triangulo (cm): ");
	scanf("%d", &l3);

	if (l1 == l2 && l1 == l3) {
		printf("El triangulo es EQUILATERO (lado1=lado2=lado3=%d)\n", l1);
	} else if (l1 == l2) {
		printf("El triangulo es ISOSCELES (lado1=lado2=%d)\n", l1);
	} else if (l1 == l3) {
		printf("El triangulo es ISOSCELES (lado1=lado3=%d)\n", l1);
	} else if (l2 == l3) {
		printf("El triangulo es ISOSCELES (lado2=lado3=%d)\n", l2);
	} else {
		printf("El triangulo es ESCALENO\n");
	}
	

	return 0;
}
```

# Ej 14.

```c
#include <stdio.h>

int main() {
	int n1, n2;

	printf("Introduzca un numero entero positivo de 3 cifras: ");
	scanf("%d", &n1);

	printf("Introduzca un numero entero positivo de 1 cifra: ");
	scanf("%d", &n2);

	if (n1 < 100 || n1 > 999) {
		printf("El primer numero no tiene 3 cifras!!!\n");
		return 0;
	}

	if (n2 < 0 || n2 > 9) {
		printf("El segundo numero no tiene 1 cifra!!!\n");
		return 0;
	} 

	printf(" %3d\n x %d\n====\n%4d\n", n1, n2, n1*n2);

	return 0;
}
```

# Ej 15.

```c
#include <stdio.h>

int main() {
	char c;

	printf("Escoga una opcion:\n");
	printf("\tB - Bebe\n");
	printf("\tA - Adolescente\n");
        printf("\tM - Mujer\n");
        printf("\tH - Hombre\n");
	printf("Opcion: ");
	scanf("%c", &c);

	switch(c) {
		case 'b':
		case 'B':
			printf("BEBE\n");
			break;
		case 'a':
		case 'A':
			printf("ADOLESCENTE\n");
			break;
		case 'm':
		case 'M':
			printf("MUJER\n");
			break;
		case 'h':
		case 'H':
			printf("Hombre\n");
			break;
		default:
			printf("Opcion incorrecta\n");
	
	}

	return 0;
}
```

# Ej 16.

```c
#include <stdio.h>

int main() {
	char name[20];
	int age;

	printf("Nombre: ");
	scanf("%s", name);

	printf("Edad: ");
	scanf("%d", &age);

	if (age < 32) {
		printf("%s es JOVEN\n", name);
	} else if (age < 75) {
		printf("%s es ADULTA\n", name);
	} else {
		printf("%s es ANCIANA\n", name);
	}

	return 0;
}
```

# Ej 17.

```c
#include <stdio.h>

int main() {
	char c;

	printf("Introduzca un caracter: ");
	scanf("%c", &c);

	if (c >= '0' && c <= '9') {
		printf("%c es un digito\n", c);
	} else if ((c >= 'a' && c <= 'z') || (c >='A' && c >= 'Z')) {
		switch (c) {
			case 'a':
			case 'A':
			case 'e':
                        case 'E':
                        case 'i':
                        case 'I':
                        case 'o':
                        case 'O':
                        case 'u':
                        case 'U':
                        	printf("%c es una vocal\n", c);
				break;
			default:
				printf("%c es una consonante\n", c);
		}

	} else {
		printf("%c es un caracter especial\n", c); 	
	}
	return 0;
}
```

# Ej 18.

```c
#include <stdio.h>

int main() {
	int aux, mod;
	int number;

	printf("Piense un numero del 1 al 10...\n");
	printf("El que ha pensado es impar (1) o par (2)? ");
	scanf("%d", &aux);

	if (aux < 1 || aux > 2) {
		printf("Dato invalido:  impar (1), par (2)\n");
		return 0;
	}

	printf("Cual es el resto de dividir el numero que ha pensado entre 5? ");
	scanf("%d", &mod);

	if (mod < 0 || mod > 4) {
		printf("Dato invalido: El valor tiene que estar entre 0 y 4\n");
		return 0;
	}

	if (aux == 2) {
		if (mod == 0) {
			number = 10;
		} else {
			number = (mod % 2 == 0 ? 0: 5) + mod;
		}
	} else {
		number = (mod % 2 == 0? 5: 0) + mod;
	}

	printf("El numero que ha pensado es el %d\n", number);

	return 0;
}
```

# Ej 19.

```c
#include <stdio.h>

int main() {
	int month, days;
	int aux;

	printf("Introduzca el numero del mes del que quiere calcular los dias [1-12]: ");
	scanf("%d", &month);

	if (month < 1 || month > 12) {
		printf("Dato invalido: el mes tiene que ser un numero comprendido entre 1 y 12\n");
		return 0;
	}

	if (month == 2) {
		printf("Introduzca el ano: ");
		scanf("%d", &aux);

		days = ((aux % 4 == 0) && (aux % 100 != 0 || aux % 400 == 0))? 28: 27;
		printf("El mes %d del ano %d tiene %d dias\n", month, aux, days);
	} else {
		aux = month < 8 ? month: (month % 8) + 1;
		days = aux % 2 == 0? 30: 31;
		printf("El mes %d tiene %d dias\n", month, days);
	}

	return 0;
}
```

# Ej 20.

```c
#include <stdio.h>
#include <math.h>

int main() {
	char name[20];
	int height;
	float weight;
	float imc;
	
	printf("Introduzca usted su nombre: ");
	scanf("%s", name);

	printf("Introduzca altura en centimetros: ");
	scanf("%d", &height);

	printf("Introduzca peso en kilos: ");
	scanf("%f", &weight);

	imc = weight / powf(height/100.0f, 2);

	printf("%s, segun el indice de masa corporal (IMC=%.2f), tiene usted ", name, imc);
	if (imc < 18.5f) {
		printf("un peso por debajo de su salud ");
	} else if (imc < 25.0f) {	
		printf("un peso normal ");
	} else if (imc < 27.0f) {
		printf("sobrepeso de grado I ");
	} else if (imc < 30.0f) {
		printf("sobrepeso de grado II ");
	} else {
		printf("obesidad ");
	}
	printf("para su estatura\n");

	return 0;
}
```

# Ej 21.

```c
#include <stdio.h>

int main() {
	const int SUBJECTS = 6;
	
	int cnt; // variable de control del bucle
	int pass; // para el sumatorio de las asignaturas aprobadas
	float grade; // la nota de una asignatura individual
	float total; // el sumatorio de todas las notas
	float aux; // para calcular las medias

	cnt = pass = 0;
	total = 0.0f;
	while (cnt < SUBJECTS) {
		printf("Introduzca calificacion asignatura %d (formato X.X, 0 para No Presentadio): ", cnt + 1);
		scanf("%f", &grade);

		if (grade >= 0.0f && grade <= 10.0f) {
			total += grade;
			if (grade != 0)
				pass++;

			cnt++;
		} else {
			printf("La calificacion tiene que estar entre 0 y 10\n");
		}
	}

	aux = total / cnt;
	printf("Nota media: ");
	switch ((int) aux) {
		case 10:
		case 9:
			printf("SOBRESALIENTE ");
			break;
		case 8:
		case 7:
			printf("NOTABLE ");
			break;
		case 6:
		case 5:
			printf("APROBADO ");
			break;
		default:
			printf("SUSPENSO ");
	}
	printf("(%.2f)\n", aux);
	
	aux = total / pass;
        printf("Nota media asignaturas presentadas: ");
        switch ((int) aux) {
                case 10:
                case 9:
                        printf("SOBRESALIENTE ");
                        break;
                case 8:
                case 7:
                        printf("NOTABLE ");
                        break;
                case 6:
                case 5:
                        printf("APROBADO ");
                        break;
                default:
                        printf("SUSPENSO ");
        }
        printf("(%.2f)\n", aux);

	return 0;
}
```

# Ej 22.

```c
#include <stdio.h>

int main() {
	const int MAX = 100;

	int even, odd;
	int cnt;

	for (cnt = even = odd = 0; cnt < MAX * 2; cnt ++)
		if (cnt % 2 == 0)
			even += cnt;
		else
			odd += cnt;

	printf("La suma de los primeros 100:\n");
	printf("\t- numeros pares es %d\n", even);
	printf("\t- numeros impares es %d\n", odd);

	return 0;
}
```

# Ej 23.

```c
#include <stdio.h>

int main() {
	const int MAX = 30;
	const int MIN = 20;

	int aux = MIN - 1;

	while (aux < MIN || aux > MAX) {
		printf("Introduce un numero entre %d y %d: ", MIN, MAX);
		scanf("%d", &aux);
	}

	printf("El numero introducido es: %d\n", aux);

	return 0;
}
```

# Ej 24.

```c
#include <stdio.h>

int main() {
	const int NUMBER = 7;
	const int MAX = 10;

	int cnt;
	for (cnt = 0; cnt <= MAX; cnt++)
		printf("%d x %2d = %2d\n", NUMBER, cnt, NUMBER * cnt);

	return 0;
}
```

# Ej 25.

```c
#include <stdio.h>

int main() {
	int row, col;
	for (row = 0; row <= 10; row++) {
		for (col = 1; col <= 5; col++)
			printf("%2d x %2d = %2d\t", col, row, col*row);
		printf("\n");
	}

	return 0;
}
```

# Ej 26.

```c
#include <stdio.h>

int main() {
	const int MAX = 10;

	int cnt, number;

	number = -1;
	while (number < 0 || number > 10) {
		printf("Introduzca un numero entero positivo: ");
		scanf("%d", &number);
	}

	for (cnt = 0; cnt <= MAX; cnt++)
		printf("%d x %2d = %d\n", number, cnt, number * cnt);

	return 0;
}
```

# Ej 27.

```c
#include <stdio.h>

int main() {
	const int MAX = 100;

	int cnt, aux;

	printf("BUCLE FOR: ");
	for (cnt =  aux = 0; cnt < MAX; cnt++)
		aux += cnt;
	printf("%d\n", aux);

	printf("BUCLE WHILE: ");
	cnt =  aux = 0;
	while (cnt < MAX) {
		aux += cnt;
		cnt++;
	}
	printf("%d\n", aux);

	printf("BUCLE DO WHILE: ");
        cnt =  aux = 0;
        do {
                aux += cnt;
                cnt++;
        } while (cnt < MAX);
        printf("%d\n", aux);

	return 0;
}
```

# Ej 28.

```c
#include <stdio.h>

int main() {
	int aux, total;

	total = 0;
	aux = -1;

	while (aux != 0) {
		printf("Introduce un numero (0 para salir): ");
		scanf("%d", &aux);

		total += aux;
	}

	printf("La suma de los numeros introducidos es %d\n", total);

	return 0;
}
```

# Ej 29.

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void print_array(int size, float a[]) {
  int cnt;

  for (cnt = 0; cnt < size; cnt++)
    printf("%s%.3f%s", (cnt == 0?"[":""), a[cnt], (cnt == size - 1?"]\n":", ") );
}


/**
 * Esta funcion leee una cadena de entrada repleta de numeros y los introduce
 * dentor de un array
 * str: cadena de texto que contiene los numeros
 * a: array donde almacenar los numeros
 * size: tamano maximo del array
 *
 * return la funcion devuelve la cantidad de numeros que se han introducido
 * dentro del array
 */
int str_to_floats(char *str, float *a, int size) {
	char delim[] ={" \t\n"}; // separadores que se usan dentro del str
	char *token;

	int cnt = 0;

	while ((token = strsep(&str, delim)) != NULL) {
		//printf("TOKEN %d <%s>\n", cnt, token);
		if (strcmp(token, "")) {
			a[cnt] = strtof(token, NULL);
			cnt++;
		}

		if (cnt == size) // No se pueden almacenar mas numeros dentro del array
			return cnt;
	}
	
	return cnt;
}

/**
 * Funcion que calcula la media de los valores de los numeros obtenidos en un
 * array de tamano size
 * array: array de numeros flotantes
 * size: cantidad de numeros a tener en cuentra dentro del array
 * return la media aritmetrica
 */
float get_average(int size, float *array) {
	int cnt;
	float aux = 0.0f;
	for (cnt = 0 ; cnt < size; cnt++)
		aux += array[cnt];

	return aux/size;
}

int main() {
	const int MAX_NUM = 10;
	const int MAX_STR = 100;
	float numbers[MAX_NUM];
	char buffer[MAX_STR];
	int size;
	printf("Introduce todos los numeros flotantes (MAX %d).\n"
	       "Los numeros han de estar separados por espacios.\n"
	       "Para finalizar pulse <enter>: \n", MAX_NUM);

	fgets(buffer, MAX_STR, stdin);

	size = str_to_floats(buffer, numbers, MAX_NUM);
	//print_array(size, numbers);	

	printf("La media aritmetrica es: %.3f\n", get_average(size, numbers));

	return 0;
}
```

# Ej 30.

```c
#include <stdio.h>

int main() {
	int aux, even, odd;

	even = odd = 0;
	aux = -1;

	while (aux != 0) {
		printf("Introduce un numero (0 para salir): ");
		scanf("%d", &aux);

		if (aux % 2 == 0)
			even += aux;
		else
			odd += aux;
	}

	printf("La suma de los numeros pares es %d\n", even);
	printf("La suma de los numeros impares es %d\n", odd);

	return 0;
}
```

# Ej 31.

```c
#include <stdio.h>

int main() {
	char c;
	int cnt;

	for (cnt = 1801; cnt < 2000; cnt++) {
		if (cnt == 1901) {
			printf("\nPulse enter para continuar...");
			scanf("%c", &c);
		}
		if (cnt % 4 == 0 && (cnt % 100 != 0 || cnt % 400 == 0))
			printf("%d ", cnt);
	}
	printf("\n");

	return 0;
}
```

# Ej 32.

```c
#include <stdio.h>

int main() {
	int value, power;
	int cnt, total;

	printf("Introduce un numero entero: ");
	scanf("%d", &value);

	printf("Introduce la potencia (positivo): ");
	scanf("%d", &power);

	if (power < 0) {
		printf("%d: la potencia tiene que ser un numero positivo\n", power);
		return 0;
	}

	total = 1;
	for (cnt = 0; cnt < power; cnt++)
		total *= value;

	printf("%d^%d = %d\n", value, power, total);

	return 0;
}
```

# Ej 33.

```c
#include <stdio.h>

int main() {
	int number;
	int cnt, total;

	printf("Introduce un numero entero positivo: ");
	scanf("%d", &number);

	if (number < 0) {
		printf("%d: el numero tiene que ser positivo\n", number);
		return 0;
	}

	total = 1;
	for (cnt = 1; cnt <= number; cnt++)
		total *= cnt;

	printf("%d! = %d\n", number, total);

	return 0;
}
```

# Ej 34.

```c
#include <stdio.h>

int main() {
	int number;
	int cnt;
	int n1, n2, n3;

	printf("Introduce un numero entero (N>0): ");
	scanf("%d", &number);

	if (number <= 0) {
		printf("%d: el numero tiene que ser positivo\n", number);
		return 0;
	}

	n1 = 0;
	n2 = 1;
	for (cnt = 2; cnt <= number; cnt++) {
		n3 = n1 + n2;
		n1 = n2;
		n2 = n3;
	}

	printf("El termino %d de la sucesion de fibonacci es %d\n", number, n3);

	return 0;
}
```

# Ej 36.

```c
#include <stdio.h>

int main() {
	char c = ' ';

	while (c != 'A' && c != 'a') {
		printf("\n");
		printf("R) Dibujar un rectangulo\n");
		printf("C) Dibujar un cuadrado\n");
		printf("P) Dibujar un pentagono\n");
		printf("A) Acabar el programa\n");
		printf("Opcion: ");
		scanf(" %c", &c);

		switch (c) {
			case 'R':
			case 'r':
				printf("Ahora dibujaria un rectangulo\n");
				break;
			case 'C':
			case 'c':
				printf("Ahora dibujaria un cuadrado\n");
				break;
			case 'P':
			case 'p':
				printf("Ahora dibujaria un pentagono\n");
				break;
			case 'A':
			case 'a':
				printf("Ahora saldre del programa\n");
				break;
			default:
				printf("%c: opcion incorrecta\n", c);
		}
	}

	return 0;
}
```

# Ej 37.

```c
#include <stdio.h>

int main() {
	int number;
	int cnt, total;
	int n1, n2, n3;

	printf("Introduce un numero entero (N>0): ");
	scanf("%d", &number);

	if (number <= 0) {
		printf("%d: el numero tiene que ser positivo\n", number);
		return 0;
	}


	if (number < 2) {
		total = number;
	} else {
		n1 = 0;
		n2 = 1;
		total = 1;
		for (cnt = 2; cnt <= number; cnt++) {
			n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
			total += n3;
		}
	}

	printf("La suma de los  %d primeros temrinos de la sucesion de fibonacci es %d\n", number, total);

	return 0;
}
```

# Ej 38.

```c
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

// 28 | 2
//   \---
// 0   14 | 2
//        \---
//      0   7 | 2
//            \---
//          1   3 | 2 
//                 \---
//              1   1

// control => para saber si estamos en los primeros zeros
// zeros => cantidad de zeros al inicio
// aux => copia de n para no perder el valor
// tmp => numero binario al reves
// result => donde se vuelca el resultado final

int main() {
	int n, result;
	int aux, tmp, mod;
	int zeros = 0;
	bool control = true;

	printf("Introduce un numero: ");
	scanf("%d", &n);

	aux = n;
	tmp = 0;
	while (aux != 0) {
		mod = aux % 2;
		if (mod == 1)
			control = false;
		else if (mod == 0 && control)
			zeros++;
		
		tmp *= 10;
		tmp += mod;

//		printf("%d", mod);
		aux /= 2;
	}
//	printf("\n");


	// Ahora hay que darle la vuelta al numero
	result = 0;
	while (tmp != 0) {
		result *= 10;
		result += tmp % 2;
		tmp /= 10;
	}
	result *= powf(10, zeros);

	if (n < 0)
		result *= -1;

	printf("%d en binario es %d\n", n, result);

	return 0;
}
```

# Ej 40.

```c
#include <stdio.h>

int main() {
	int cnt;
	float sum, lim;

	printf("Introduzca un limite para la sucesion: ");
	scanf("%f", &lim);

	cnt = 0;
	sum = 0.0f;
	while (sum <= lim) {
		cnt++;
		sum += 1/ (float) cnt;
	}

	printf("Se han realizado %d iteraciones para alcanzar el valor %f (limite=%f)\n", cnt, sum, lim);	

	return 0;
}
```

# Ej 41.

```c
#include <stdio.h>

int main() {
	const int MAX = 1000;
	int x, y, z;

	for (x = 0; x <= MAX; x++)
		for (y = 0; y <= MAX; y++)
			for (z = (x<y)? x: y; z <= MAX; z++)
				if (x*x + y*y == z*z)
					printf("%d^2 + %d^2 = %d^2\n", x, y, z);

	return 0;
}
```

# Ej 43.

```c
#include <stdio.h>
#include <math.h>

#include <unistd.h>

int main() {
	const float ERROR = 0.0001f;
	float x, sum, value;
	int cnt, fact;

	printf("Introduzca el valor de x para calcular e^x: ");
	scanf("%f", &x);

	fact = cnt = 1;
	sum = cnt;
	value = expf(x);
	while (fabsf(sum - value) > ERROR) {
		if (cnt % 10 == 9)
			printf("%d) value=%f sum=%f (error=%f)\n", cnt, value, sum, value - sum);
		fact *= cnt;
		sum += powf(x, cnt)/fact;
		cnt++;
	}
	printf("Se han realizado %d iteraciones para alcanzar el valor de e^%f=%f\n", cnt, x, sum);

	return 0;
}
```

# Ej 46.

```c
#include <stdio.h>

int main() {
	const int MAX = 10;
	int row, col;

	// header
	for (col = 1; col < MAX; col++)
		printf("\t%d", col);
	printf("\n\n");

	for (row = 1; row < MAX; row++) {
		printf("%d", row);
		for (col = 1; col <= row; col++)
                	printf("\t%d", row*col);
        	printf("\n\n");
	}

	return 0;
}
```

# Ej 47.

```c
#include <stdio.h>

int main() {
	int n, cnt, row, col;

	printf("Introduce el numero de filas: ");
	scanf("%d", &n);

	for (cnt = row = 1; row <= n; row++) {
		for (col = 1; col <= row; col++) {
			printf("%d\t", cnt);
			cnt++;
		} 
		printf("\n");
	}

}
```

# Ej 48.

```c
#include <stdio.h>

int main() {
	int n, cnt, row, col;

	printf("Introduce el numero donde parar: ");
	scanf("%d", &n);

	for (cnt = row = 1; cnt <= n; row++) {
		for (col = 1; col <= row && cnt<= n; col++) {
			printf("%d\t", cnt);
			cnt++;
		} 
		printf("\n");
	}

}
```
