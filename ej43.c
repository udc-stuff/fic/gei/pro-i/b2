#include <stdio.h>
#include <math.h>

#include <unistd.h>

int main() {
	const float ERROR = 0.0001f;
	float x, sum, value;
	int cnt, fact;

	printf("Introduzca el valor de x para calcular e^x: ");
	scanf("%f", &x);

	fact = cnt = 1;
	sum = cnt;
	value = expf(x);
	while (fabsf(sum - value) > ERROR) {
		if (cnt % 10 == 9)
			printf("%d) value=%f sum=%f (error=%f)\n", cnt, value, sum, value - sum);
		fact *= cnt;
		sum += powf(x, cnt)/fact;
		cnt++;
	}
	printf("Se han realizado %d iteraciones para alcanzar el valor de e^%f=%f\n", cnt, x, sum);

	return 0;
}
