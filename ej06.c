#include <stdio.h>

int main() {
	int question, correct;
	float aux;

	printf("Cantidad total de preguntas: ");
	scanf("%d", &question);
	printf("Cantidad de preguntas correctas: ");
	scanf("%d", &correct);

	aux = (float) correct/question;

	printf("Porcentaje de respuestas correctas %.2f\n", aux * 100);
	if (aux >= 0.9f){
		printf("Nivel maximo (>=90%%)\n");
	}  else if (aux >= 0.75f){
		printf("Nivel medio (>=75%% y <90%%)\n");
	} else if (aux >= 0.5f){
		printf("Nivel regular (>=50%% y < 75%%)\n");
	} else {
		printf("Fuera de nivel (<50%%)\n");
	}

	return 0;
}
