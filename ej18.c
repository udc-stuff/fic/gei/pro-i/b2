#include <stdio.h>

int main() {
	int aux, mod;
	int number;

	printf("Piense un numero del 1 al 10...\n");
	printf("El que ha pensado es impar (1) o par (2)? ");
	scanf("%d", &aux);

	if (aux < 1 || aux > 2) {
		printf("Dato invalido:  impar (1), par (2)\n");
		return 0;
	}

	printf("Cual es el resto de dividir el numero que ha pensado entre 5? ");
	scanf("%d", &mod);

	if (mod < 0 || mod > 4) {
		printf("Dato invalido: El valor tiene que estar entre 0 y 4\n");
		return 0;
	}

	if (aux == 2) {
		if (mod == 0) {
			number = 10;
		} else {
			number = (mod % 2 == 0 ? 0: 5) + mod;
		}
	} else {
		number = (mod % 2 == 0? 5: 0) + mod;
	}

	printf("El numero que ha pensado es el %d\n", number);

	return 0;
}
